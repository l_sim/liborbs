# Library for Orbital Manipulations

In this library we would like to define:

1. The specification of an orbital, in most of the cases defined into a localized domain, which can be discretized in

   a. Wavelet basis (wrapping libconv)

   b. Gaussian basis
   
   c. Real-space

  Such discretization may be exclusive or inclusive (towards mixed basis approaches). 
  Also, the boundary condition at the domain border may be mixed (as well as monoclinic when applicable)

2. The calculation of the scalar product between sets of orbitals defined that way.

3. The communications of basis of orbitals between processes.

4. The application of basis Hamiltoninan operators (convolutions, kinetic terms, real-space potentials...)
this would enable:

   a. Traditional KS operators (including k-point)

   b. Complex basis sets (non hermitian hamitonians)

   c. Meta-GGAs

   d. Confining terms (for e.g. preconditioner)

5. I/O of such basis terms.

 The idea of the library is to provide API for fleixible definition of SCF operation at the top level,
 where the developer of the SCF algorithm is not supposed to deal with the internal representation of
 the orbitals.
 A succesful API implementation would be such that the user is unaware of the basis sets employed for the discretization.

6. Internal conversion between the various basis representation
  
   a. conversion for gaussians to wavelets (NL PSP, mixed basis sets)

   b. From wavelets to real-space (density calculation)

   c. from gaussians to real-space (for non-othrhombic)
 
   d. From gaussians to wavelets (but in a mixed-basis intention)

7. Performances should be carefully benchamrked.

   a. MPI and OMP behaviour

   b. CPU as well as GPU usage (wrapping of convolutions)

   c. MPI scalablity of the communication patterns (load balancing of orbitals distributions)
